import {Component, Input, OnInit} from '@angular/core';
import {OrderLine} from '../order-line';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Product} from '../product';

@Component({
  selector: '[app-order-display-line]',
  templateUrl: './order-display-line.component.html',
  styleUrls: ['./order-display-line.component.css']
})
export class OrderDisplayLineComponent implements OnInit{
  @Input() line: OrderLine;
  product: Product = null;
  error: HttpErrorResponse = null;

  constructor(private http: HttpClient){
  }

  ngOnInit() {
    this.getProductBySku(this.line.sku);
  }

  getProductBySku(sku: number): void {
    this.http.get<Product>('http://localhost:8080/products/' + sku)
      .subscribe(data => {
          this.product = data;
        },
        (error: HttpErrorResponse) => {
          this.error = error;
        }
      );
  }}
