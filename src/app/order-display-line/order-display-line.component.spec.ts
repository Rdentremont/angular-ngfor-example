import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDisplayLineComponent } from './order-display-line.component';

describe('OrderDisplayLineComponent', () => {
  let component: OrderDisplayLineComponent;
  let fixture: ComponentFixture<OrderDisplayLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDisplayLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDisplayLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
