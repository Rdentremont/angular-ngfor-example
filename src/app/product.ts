export class Product {
  sku: number;
  unitPrice: number;
  description: string;
}
