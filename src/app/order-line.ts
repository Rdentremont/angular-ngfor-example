export class OrderLine {
  sku: number;
  quantity: number;
}
