import {Component, Input} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {OrderLine} from './order-line';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'order-lines-example';
  lines: Array<OrderLine> = null;
  error: HttpErrorResponse = null;

  constructor(private http: HttpClient){
  }

  getOrderByNumber(number: number): void {
    this.error = null;
    this.lines = null;
    this.http.get<Array<OrderLine>>('http://localhost:8080/orders/' + number + '/lines')
      .subscribe(data => {
          this.lines = data;
          console.log(this.lines);
        },
        (error: HttpErrorResponse) => {
          this.error = error;
        }
      );
  }
}
